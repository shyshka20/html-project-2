let menu = document.getElementById('menu')
menu.onclick = function myFunction() {
    if (document.querySelector('.nav')) {
        let x = document.getElementById('topnav')
        x.className = " responsive"
        menu.innerHTML = '&#9746;'
    } else {
        let y = document.querySelector('.responsive');
        menu.innerHTML = '&#9776;'
        y.className = " nav"
    }

}
const rightBtn = document.querySelector('.border2');
const leftnBtn = document.querySelector('.border');
const sliderImage = document.getElementById('slider_image');
const radioButtons = document.querySelectorAll('.radio');
const SlideCount = 5;
let activeSlideindex = 1;
rightBtn.addEventListener('click', () => {
    changeSlide('right');
})
leftnBtn.addEventListener('click', () => {
    changeSlide('left')
})
function changeSlide(direction) {
    if (direction === 'right') {
        activeSlideindex++
        if (activeSlideindex > SlideCount) {
            activeSlideindex = 1;
        }
        console.log(activeSlideindex)
    } else if (direction === 'left') {
        activeSlideindex--
        if (activeSlideindex < 1) {
            activeSlideindex = SlideCount;
        }
    }
    changeRadioButton()
    sliderImage.src = `${activeSlideindex}.jpeg`
}
changeRadioButton()
radioButtons.forEach(el => el.addEventListener('click', () => {
  changeImage();
}))

function changeImage() {
    radioButtons.forEach(el => {
        if (el.checked)
           activeSlideindex = +el.value;
    })
    changeSlide();
}
function changeRadioButton() {
    for (let i=0; i<radioButtons.length;i++){
        radioButtons[activeSlideindex-1].checked = true;
    }
}
changeRadioButton()